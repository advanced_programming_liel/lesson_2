1. .open magshimim.db
create table StudentsInClass(studentID int, classID int);
create table Teachers(teacherID int, teacherName string);
create table Categories(catID int, catName string);
create table classes(classID int, className string, teacherID int, catID int);
create table Students(studentID int, studentName string, studentAge int, studentGrade string);
2. (1) update students set studentAge = 17 where studentName = "David" or studentName = "Mike";
(2) update students set studentGrade = 93 where studentName = "Anat";
(3) insert into students(studentID, studentName, studentAge, studentGrade) values(11, "Liel", 16, 100);
insert into students(studentID, studentName, studentAge, studentGrade) values(12, "Sagi", 12, 100);
insert into students(studentID, studentName, studentAge, studentGrade) values(13, "Shem", 18, 5);
(4) insert into classes(classID, className, teacherID, catID) values(12, "Sleeping", 5, 3);
(5) insert into Teachers(teacherID, teacherName) values(8, "Orit"
(6) update classes set teacherID = 8 where className = "Sleeping";
(7) insert into classes(classID, className, teacherID, catID) values(13, "Education", 7, 4);
(8) insert into StudentsInClasses(studentID, classID) values(11, 12);
insert into StudentsInClasses(studentID, classID) values(12, 12);
insert into StudentsInClasses(studentID, classID) values(13, 12);
(9) select COUNT(*) from teachers;
(10) select AVG(studentGrade) from students;
3. begin transaction;
update accounts set balance = 900 where id = 3;
update accounts set balance = -100 where id = 1;
commit;
begin transaction;
update accounts set balance = 500 where id = 2;
update accounts set balance = 100 where id = 1;
commit;

bonus:
 begin transaction;
insert into accounts(id, balance) values(4, 500);
update accounts set balance = 0 where id = 2;
commit;
begin transaction;
insert into accounts(id, balance) values(5, 900);
update accounts set balance = 0 where id = 3;
commit;
alter table accounts add column name string;
update accounts set name = "Liel" where id = 1;
update accounts set name = "Aaron" where id = 2;
update accounts set name = "Or" where id = 3;
update accounts set name = "Tomer" where id = 4;
update accounts set name = "Idan" where id = 5;
select name, balance from accounts;
 select name as "ClientName", balance as "ClientBalance" from accounts;